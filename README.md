# List 5 truyện ngôn tình hoàn full hay mới update

Đối với các nàng yêu thích đọc ngôn tình, không gì sung sướng hơn là được đón đọc một bộ truyện ngôn tình full hợp ý. Và đây, list 5 [**truyện ngôn tình full - ngôn tình hoàn hay nhất**](https://mstory.vn/the-loai/ngon-tinh/hoan/) update sớm nhất sẽ đưa ra cho các nàng những lựa chọn mới. Hãy đọc thử và cảm nhận xem đâu là câu chuyện hay ho và cuốn hút nhất nhé.  


## 1\. Giao ước

*   Tác giả: An Viên
*   Thể loại: ngôn tình, linh dị
*   Số chương: 41

[![](https://imgice.com/images/2018/10/05/1.png)](#)

*Giao ước là truyện hay của tác giả An Viên*

Sau một tai nạn khiến chính mình suýt chết hụt, Hướng Dương – một cô gái yêu đời, đã hoàn toàn thay đổi. Kể từ ngày đó, cô bị mất ngủ triền miên và bắt đầu cảm nhận được khả năng bí ẩn của bản thân, đó là nhìn thấy các linh hồn và ma quỷ. Chính vì khả năng khác người này, cô trở nên buồn bã, u ám và khép kín với cuộc sống.  

Cùng thời điểm đó, tại một khu rừng biệt lập ngoại ô thành phố, có một người con trai là thiếu gia nhà giàu nổi tiếng bị tai nạn giao thông chết cách đõ hai năm trên một đoạn đường hoang vắng.  

Đoạn đường đó có tên là giao lộ âm dương.  

Bà mẹ thương con, muốn tìm một người vợ cho con trai nên đã nhờ một bà thầy bói nổi tiếng ở Trung Quốc chuyên làm mối cho người chết, làm minh hôn cho con trai mình.  

Hướng Dương bị ruồng rẫy vì gia đình không chấp một đứa con gái có khả năng kỳ lạ như cô. Cô đi lang thang một mình trên đường mà không hay mình đã đặt chân vào giao lộ âm dương, tình cờ gặp bà thầy bói đang chuẩn bị làm minh hôn cho thiếu gia nhà giàu. Và từ đây, cuộc đời của cô đã lật sang một trang khác.  



## 2\. Sống lại, độc đế đuổi thê

*   Tác giả: Dịch Năm
*   Thể loại: [**ngôn tình**](https://plus.google.com/u/1/112101129833654686439/posts/hwMVWho71xw), xuyên không, trọng sinh, sủng
*   Số chương: 130

Đang cùng biểu tỷ thảo luận kịch tính của tiểu thuyết, vậy mà chỉ một giây sau đó, linh hồn của cô lại xuyên vào cuốn tiểu thuyết mình cùng biểu tỷ vừa nói đến. Cô biến thành muội muội sinh đôi của nữ chủ đa tình trong trang truyện.  

Tỷ tỷ ai gặp cũng yêu thích, nữ xứng muội muội hẳn cũng được dính chút ánh hào quang chứ?  

Hào quang còn chưa thấy đâu, đi theo tỷ tỷ, cô thế mà bị một biến thái cuồng ngược một trận.  

Hỗn đản, cô thể nhưng là người có bàn tay vàng trong tiểu thuyết đó! Ngược cô sẽ bị cô ngược lại sống không bằng chết luôn!  



## 3\. Nếu còn có ngày mai 

*   Tác giả: Khoa Tâm Nhi
*   Thể loại: ngôn tình, sắc, ngược, sủng, NP
*   Số chương: 60

"Triết, xin anh đừng đối xử như vậy với em!"  

"Tôi giao cô ta cho các anh, thoải mái mà chơi chết cô ta đi "  

"Hóa ra vẫn còn là gái trinh"  

Mọi chuyện bắt đầu mất kiểm soát khi giữa cô và anh xuất hiện người thứ ba. Anh nghe lời người thứ ba và nghĩ cô là người phụ nữ độc ác, giúp người kia trả thù cô. Anh sẽ cảm thấy sao khi thấy người kia lên giường cùng tên đàn ông khác? Liệu anh có ý thức được rằng Ngôn Hi xinh đẹp hiền lành anh từng yêu thương đã hoàn toàn biến mất khỏi cuộc đời anh.  

Nếu còn có ngày mai, liệu anh có quay đầu lại? Nếu còn có ngày mai, liệu cô sẽ làm gì để tiếp tục sống?  



## 4\. Tòa thành bị vùi lấp

*   Tác giả: Thiên Như Ngọc
*   Thể loại: Hiện đại, nam nữ cường, sạch, sủng, sắc, HE
*   Số chương: 79

Hai con người ở hai thế giới khác nhau, nhiều sự đối lập trong tính cách và hoàn cảnh sống, vậy mà cứ như hai thỏi nam châm trái chiều hút lấy nhau, không cách nào tách rời, dù chạy đến chân trời góc bể rồi vẫn lại về bên nhau.  

Quan Dược, đội trưởng đội khảo cổ, lấy danh nghĩa là người bảo vệ di cổ, nhưng lại là một con sói thâm tang bất lộ. Anh rất men, cao to mạnh mẽ, chân thành chính trực, sống ngần ấy năm mà chưa biết đến tính ái, thế nên chẳng chịu nổi mà lại không chịu nổi việc một con mèo hoang như Ngôn Tiêu quanh quẩn bên mình trêu chọc, cào vuốt.  

Còn cô, Ngôn Tiêu, là một cô nhi được nhận nuôi, tình cảm với cha mẹ nuôi chưa kịp mặn mà lại bị bỏ rơi ngay khi 16 tuổi. Cô đã sống những tháng ngày bị người ta ghen ghét, bắt nạt, nghèo đến mức phải lưu lạc gầm cầu, ăn mì tôm để cầm cự qua ngày. Sau khi nhận lại số tài khoản, đánh người chú chiếm gia tài kia thiếu mức chết đi sống lại, kiện hắn ra tòa, … rồi lại cô độc một mình vùng vẫy ở mảnh đất Tây Bắc. Vốn ban đàu chỉ định trêu chọc Quan Dược, nhưng cuối cùng lại chẳng thể dứt ra được nữa.  

Tây Bắc, nơi một câu chuyện tình yêu đẹp diễn ra ngập trong sa mạc cát lạnh nhưng lại vô cùng ấm áp ngọt ngào.  



## 5\. Cô vợ nhỏ của tổng tài phúc hắc 

*   Tác giả: NoharaMia
*   Thể loại: Sủng, HE, hắc bang, ngôn tình, lãng mạn
*   Số chương: 64

[![](https://imgice.com/images/2018/10/05/2.png)](#)

*Cô vợ nhỏ của tổng tài phúc hắc*

Mồ côi cha mẹ từ nhỏ nhưng cô may mắn hơn bao người khác, được nhận nuôi bởi một gia đình khá giả và hứa hôn cho con trai của họ.  

Cả hai yêu đương được 3 năm thì đến một ngày, cô bất ngờ nhận được tin báo người yêu đang quan hệ với cô gái khác ở khách sạn. Vội vàng chạy đến đó để xác thực. Chứng kiến mọi thứ làm cô sụp đổ. Dọn hành lí, cô bắt đầu một cuộc sống mới ở chung cư của mình. Và ở đó, họ gặp nhau.  

Hai con người với tính cách khác biệt hoàn toàn, họ đã đến với nhau thế nào? Điều gì đã khiến cả hai lại gần nhau? Kết cục liệu có hoàn mỹ cho cả hai?  

Hãy đọc và cảm nhận những điều ngọt ngào trong từng trang [**truyện ngôn tình hoàn hay**](https://www.scoop.it/t/truyen-mobile/p/4101849506/2018/09/23/truyen-ngon-tinh-full-hoan-hay-nhat) mà tác giả mở ra trước mắt mỗi chúng ta.